// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

#include "InternetButton.h"
#include "math.h"


InternetButton b = InternetButton();


// variable to track jumps
int jumps = 0;
bool jumped = false;
int targetJumps;

 
void setup() {


b.begin();
 b.setBrightness(50);


 Particle.function("JumpFunction", didJump);

}

void loop() {
    
    
    if(jumped == true){
        delay(10);
        activateLeds();
        jumped = false;
    }
    
}

void activateLeds() {

    b.allLedsOff();
    
    // using only y axis value to monitor jump
    int yValue = b.readY();
    int xvalue = b.readX();
   
   
    if(abs(yValue) * 100 > 700){
       
        jumps++;
    
    
   for(int i = targetJumps; i >= jumps; i--){
         b.ledOn(i, 0, 250, 4);
    }

    }
}

int didJump(String command){
    //parse the string into an integer
    int howManyJumps = atoi(command.c_str());


    targetJumps = howManyJumps;
    
     jumped = true;
 
 return 1;

}